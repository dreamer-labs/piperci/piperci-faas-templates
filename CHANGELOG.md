# [1.6.0](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/compare/v1.5.0...v1.6.0) (2020-03-11)


### Features

* system package installation support ([699f1b4](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/commit/699f1b4))

# [1.5.0](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/compare/v1.4.0...v1.5.0) (2020-01-31)


### Features

* logging file support ([07353c7](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/commit/07353c7))

# [1.4.0](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/compare/v1.3.0...v1.4.0) (2020-01-30)


### Features

* updated testing and fixed input validation bugs ([c07e681](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/commit/c07e681))

# [1.3.0](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/compare/v1.2.0...v1.3.0) (2020-01-22)


### Features

* update models and improve test with less mocking ([23913a3](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/commit/23913a3))

# [1.2.0](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/compare/v1.1.1...v1.2.0) (2020-01-10)


### Features

* gateway and executor now called in from path ([7acf91d](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/commit/7acf91d))

## [1.1.1](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/compare/v1.1.0...v1.1.1) (2019-12-10)


### Bug Fixes

* Fix custom schema inheritence ([940f168](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/commit/940f168))

# [1.1.0](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/compare/v1.0.0...v1.1.0) (2019-12-06)


### Features

* Add validation functionality ([6031256](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/commit/6031256))

# 1.0.0 (2019-09-11)


### Bug Fixes

* rename request field to requests, which broke downstream builds ([6e97791](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/commit/6e97791))


### Features

* Provide common interface for both internal and external validation ([bef2c28](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/commit/bef2c28)), closes [#5](https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/issues/5)
