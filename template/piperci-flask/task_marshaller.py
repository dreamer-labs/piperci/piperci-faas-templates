from .marshaller import Marshaller, MarshallError
from .models import task_schema_gen

from marshmallow import ValidationError


class TaskMarshaller(Marshaller):
    def __init__(self, raw):
        super(TaskMarshaller, self).__init__(raw)
        self._task = None
        self._schema = task_schema_gen()

        self._request = None

    def enforce(self, context):  # noqa: C901

        if context == "pre_task":

            try:
                self._task = self._schema().load(self.raw_data)
            except ValidationError as e:
                self.errors.extend(e.messages, data=self.raw_data)

        if len(self.errors.errors):
            raise MarshallError(self.errors)

    @property
    def task(self):
        return self._task
