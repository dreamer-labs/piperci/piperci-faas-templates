"""This module is for testing and validation only
    and does not represent a real for a PiperCI Task
"""

from function.faas_app import app
from gevent.pywsgi import WSGIServer


if __name__ == "__main__":
    http_server = WSGIServer(("", 5000), app)
    http_server.serve_forever()
