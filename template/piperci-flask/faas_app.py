import os
import tempfile
import traceback

from flask import Flask, request
from piperci.task.exceptions import PiperError
from piperci.task.this_task import ThisTask, init_logger
from werkzeug.exceptions import BadRequest

from .config import Config
from . import handler
from .marshaller import MarshallError
from .task_marshaller import TaskMarshaller


app = Flask(__name__)


@app.before_request
def fix_transfer_encoding():
    """
    Sets the "wsgi.input_terminated" environment flag, thus enabling
    Werkzeug to pass chunked requests as streams.  The gunicorn server
    should set this, but it's not yet been implemented.
    """
    transfer_encoding = request.headers.get("Transfer-Encoding", None)
    if transfer_encoding == "chunked":
        request.environ["wsgi.input_terminated"] = True


@app.route("/", defaults={"path": ""}, methods=["POST"])
@app.route("/<path:path>", methods=["POST"])
def main_route(path):
    start_dir = os.getcwd()

    func_type = "executor" if "executor" in path else "gateway"

    try:
        with tempfile.TemporaryDirectory() as tempdir:
            os.chdir(tempdir)
            log_file = os.path.join(tempdir, "piperci_task.log")
            logger = init_logger(log_file=log_file)

            logger.info(f"starting request for an {func_type}")
            Config["path"] = path
            Config["endpoint"] = request.headers.get("X-Forwarded-Host")
            Config["log_file"] = log_file

            try:
                marshaller = TaskMarshaller(request.get_json(force=True))
                marshaller.enforce("pre_task")
                logger.info("marshalled input")
            except BadRequest:
                logger.info("no JSON in request")
                return ({"errors": {"faas_app:main_route":
                                    ["JSON must be supplied in request"]}},
                        400)
            except MarshallError as e:
                logger.info("failed to marshal input")
                return e.errors.emit(), 422
            except Exception:
                error = traceback.format_exc()
                return {"errors": {
                    "faas_app:main_route":
                        [f"Unknown error processing function. \n{error}"]}}, 400
            try:
                data = {
                    "storage": {
                        "storage_type": "minio",
                        "access_key": Config["storage"]["access_key"],
                        "secret_key": Config["storage"]["secret_key"],
                        "hostname": Config["storage"]["url"],
                    },
                    "caller": Config["name"],
                    "gman_url": Config["gman"]["url"],
                    "status": "started" if func_type == "gateway" else "received",
                    "thread_id": (str(marshaller.task["thread_id"])
                                  if marshaller.task["thread_id"] else None),
                    "parent_id": (str(marshaller.task["parent_id"])
                                  if marshaller.task["parent_id"] else None),
                    "project": marshaller.task["project"],
                    "run_id": marshaller.task["run_id"],
                    "stage": marshaller.task["stage"],
                    "log_file": log_file
                }
                task = ThisTask(**data)
            except PiperError as e:
                return {"errors": {
                    "faas_app:main_route":
                        [f"There was an error creating task {e}"]}}, 400

            if marshaller.task["only_validate"]:
                return task.complete("Validation completed successfuly")
            Config["task_config"] = marshaller.task
            try:
                logger.info(f"calling handler.{func_type}")
                if func_type == "executor":
                    return handler.executor(request=request, task=task, config=Config)
                else:
                    return handler.gateway(request=request, task=task, config=Config)
            except Exception:
                error = traceback.format_exc()
                return task.fail(f"Unknown error processing function. \n{error}")

    finally:
        os.chdir(start_dir)
