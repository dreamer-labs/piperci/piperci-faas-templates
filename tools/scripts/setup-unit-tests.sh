#!/bin/bash

FUNCTION_TEMPDIR=$1
echo "module root:"
echo $FUNCTION_TEMPDIR


THIS_SCRIPT=$(basename "${BASH_SOURCE[0]}")
THIS_RELDIR=$(dirname "${BASH_SOURCE[0]}")
REPO_BASEDIR=$(cd $THIS_RELDIR; cd ../../; pwd)

[[ -d ${FUNCTION_TEMPDIR} ]] && rm -r ${FUNCTION_TEMPDIR};

mkdir ${FUNCTION_TEMPDIR}

cp -rv ${REPO_BASEDIR}/template ${FUNCTION_TEMPDIR}
touch ${FUNCTION_TEMPDIR}/template/__init__.py
mv ${FUNCTION_TEMPDIR}/template/piperci-flask/faas_app.py \
  ${FUNCTION_TEMPDIR}/template/piperci-flask/function/
mv ${FUNCTION_TEMPDIR}/template/piperci-flask/models.py \
  ${FUNCTION_TEMPDIR}/template/piperci-flask/function/
mv ${FUNCTION_TEMPDIR}/template/piperci-flask/task_marshaller.py \
  ${FUNCTION_TEMPDIR}/template/piperci-flask/function/
mv ${FUNCTION_TEMPDIR}/template/piperci-flask/marshaller.py \
  ${FUNCTION_TEMPDIR}/template/piperci-flask/function/
mv ${FUNCTION_TEMPDIR}/template/piperci-flask \
  ${FUNCTION_TEMPDIR}/template/piperci_flask

cp -r ${REPO_BASEDIR}/tests ${FUNCTION_TEMPDIR}/tests
