import json
import os
import pytest
import responses
import sys

from typing import Tuple
from urllib.parse import urlparse

from marshmallow import Schema, RAISE, fields

from marshmallow.exceptions import ValidationError

from piperci.storeman.client import storage_client

try:
    sys.path.insert(0, os.environ["MODULE_ROOT"])
except KeyError:
    pass

from template.piperci_flask.function.marshaller import MarshallError
from template.piperci_flask.function.task_marshaller import TaskMarshaller


@pytest.fixture
def single_task():
    return {
        "project": "A Test Project",
        "run_id": "1234",
        "stage": "tox_tests",
        "thread_id": "5c96f41a-3614-4b0b-a83c-a700fc75d5b9",
        "task_id": "5c96f41a-3614-4b0b-a83c-a700fc75d5b9",
        "parent_id": "ed450ab7-38b2-425b-9bc6-c057ebc9b39a",
        "caller": "test_case_create_1"
    }


@pytest.fixture
def single_task_run(single_task):
    task_run = single_task.copy()
    task_run["artifacts"] = [
        "sha384-oqVuAfXRKap7fdgcCY5uykM6+R9GqQ8K/uxy9rx7HNQlGYl1kPzQho1wx4JwY8wC"
        ]
    task_run["config"] = {}
    return task_run


@pytest.fixture
def start_task_response(config, single_task) -> Tuple[Tuple, dict]:
    return (
        (responses.POST, f"{config['gman']['url']}/task"),
        {
            "task": single_task
        },
    )


@pytest.fixture
def task_response(config, single_task) -> Tuple[Tuple, dict]:
    return (
        (
            responses.PUT,
            f"{config['gman']['url']}/task/{single_task['task_id']}",
        ),
        {
            "timestamp": "2019-07-17T12:10:32.952267+00:00",
            "event_id": "9038675b-66ad-4d8f-9c38-a3fa5a93db8c",
            "task": {
                "parent_id": None,
                "run_id": "create_1",
                "project": "gman_test_data",
                "thread_id": "{single_task['thread_id']}",
                "caller": "test_case_create_1",
                "task_id": "{single_task['task_id']}",
            },
            "return_code": None,
            "status": "started",
            "message": " task creation body",
        },
    )


@pytest.fixture
def task_artifact_upload_response(config) -> Tuple[Tuple, dict]:
    return (
        (responses.POST, f"{config['gman']['url']}/artifact"),
        {
            "status": "unknown",
            "uri": "https://someminio.example.com/art1",
            "artifact_id": "884053a3-277b-45e4-9813-fc61c07a2cd6",
            "type": "artifact",
            "sri": "sha256-sCDaaxdshXhK4sA/v4dMHiMWhtGyQwA1fP8PgrN0O5g=",
            "task": {
                "task_id": "a9a1ca15-747d-43f9-8f04-1a66de8fef33",
                "caller": "test_case_create_1",
                "project": "gman_test_data",
                "thread_id": "a9a1ca15-747d-43f9-8f04-1a66de8fef33",
                "run_id": "create_1",
            },
            "event_id": "a48efe28-db9e-4330-93c4-5f480b2bef71",
        },
    )


@pytest.fixture
def task_creation_response(config) -> Tuple[Tuple, dict]:
    return (
        (
            responses.PUT,
            f"{config['gman']['url']}/task/9c4d3068-fa2d-4cdf-ae10-731c82cd7d2d",
        ),
        {
            "timestamp": "2019-07-17T12:10:32.952267+00:00",
            "event_id": "9038675b-66ad-4d8f-9c38-a3fa5a93db8c",
            "task": {
                "parent_id": None,
                "run_id": "create_1",
                "project": "gman_test_data",
                "thread_id": "9c4d3068-fa2d-4cdf-ae10-731c82cd7d2d",
                "caller": "test_case_create_1",
                "task_id": "9c4d3068-fa2d-4cdf-ae10-731c82cd7d2d",
            },
            "return_code": None,
            "status": "started",
            "message": " task creation body",
        },
    )


@pytest.fixture
def minio_server(s3_server):
    server_url = s3_server.boto_endpoint_url
    return urlparse(server_url).netloc


@pytest.fixture
def gman_client_mock():

    class _gman_mock(object):

        def get_run(*args, **kwargs):
            return kwargs["run_id"]

        def get_thread(*args, **kwargs):
            return kwargs["thread_id"]

        def get_task(*args, **kwargs):
            return kwargs["task_id"]

    return _gman_mock


@pytest.fixture
def common_headers(request_ctx):
    return {"Content-Type": "application/json",
            "X-Forwarded-Host": request_ctx.request.host_url}


@pytest.fixture
def task_delegation_response(config):
    introspect = {"json": {}}

    def callback(request):
        body = json.loads(request.body)
        introspect["json"] = body
        try:
            marshaller = TaskMarshaller(body)
            marshaller.enforce("pre_task")
            introspect["marshaller"] = True
            return 200, {}, json.dumps({"success": "valid json"})
        except MarshallError as e:
            introspect["marshaller"] = False
            return 422, {}, json.dumps(e.errors.emit())
        except Exception as e:
            introspect["marshaller"] = e
            return (400, {}, json.dumps({"errors": [{
                "conftest:delegation_respons":
                    f"Unknown error processing function. \nUnknown"}]})
                    )

    return {
        "args": [responses.POST,
                 config["executor_url"].format(endpoint=config["endpoint"])],
        "kwargs": {"callback": callback},
        "introspect": introspect
        }


@pytest.fixture
def task_gman_response(config, single_task):
    introspect = {"json": {}}

    class GmanCreateTask(Schema):

        project = fields.Str(required=True)
        thread_id = fields.UUID(required=False,
                                default=None,
                                missing=None)
        parent_id = fields.UUID(required=False)
        run_id = fields.Str(required=True)
        caller = fields.Str(required=True)
        status = fields.Str(required=True)
        message = fields.Str(required=True)

        class Meta:
            unknown = RAISE

    def callback(request):
        body = json.loads(request.body)
        introspect["json"] = body
        try:
            GmanCreateTask().load(body)
            introspect["marshaller"] = True
            return 200, {}, json.dumps({"task": single_task})
        except ValidationError as e:
            introspect["marshaller"] = False
            return 422, {}, json.dumps({"errors": e.messages})
        except Exception as e:
            introspect["marshaller"] = e
            return (400, {}, json.dumps({"errors": [{
                "conftest:conftest_gman_response":
                    f"Unknown error processing function"}]})
                    )
    return {
        "args": [responses.POST, f"{config['gman']['url']}/task"],
        "kwargs": {"callback": callback},
        "introspect": introspect
        }


@responses.activate
@pytest.fixture
def standard_environ(
    minio_server,
    mocker,
    gman_client_mock,
    start_task_response,
    task_response,
    task_artifact_upload_response,
    task_creation_response,
    task_delegation_response,
    task_gman_response
):
    responses.add(*task_response[0], json=task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    responses.add_callback(*task_delegation_response["args"],
                           **task_delegation_response["kwargs"])

    responses.add_callback(*task_gman_response["args"],
                           **task_gman_response["kwargs"])

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
        storage_type="minio",
        hostname=minio_server,
        access_key="MINIO_TEST_ACCESS",
        secret_key="MINIO_TEST_SECRET",
    )

    mocks = {}
    mocks["storage"] = mocker.patch(
        "piperci.task.this_task.ThisTask._init_storage_client", store_cli)
    mocks["gman_client"] = mocker.patch(
        "template.piperci_flask.function.models.gman_client", gman_client_mock())

    return {
        "task_delegation_response": task_delegation_response,
        "mocks": mocks
        }
