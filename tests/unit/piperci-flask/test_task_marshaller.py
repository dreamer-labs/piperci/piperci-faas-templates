from template.piperci_flask.function.task_marshaller import TaskMarshaller


def test_new_task_property_is_none(single_task_run):
    tasks = TaskMarshaller(single_task_run)

    assert not tasks.task
