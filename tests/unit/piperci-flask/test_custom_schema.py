import json
import os
import pytest
import responses
import sys
import textwrap

from template.piperci_flask.function.marshaller import MarshallError
from template.piperci_flask.function.task_marshaller import TaskMarshaller


@pytest.fixture(scope="function")
def replace_file():
    custom_schema = (
        f"{os.environ['MODULE_ROOT']}/template/piperci_flask/function/custom_schema.py"
    )

    with open(custom_schema, "w+") as f:
        f.write(
            textwrap.dedent(
                """
            from marshmallow import fields, Schema, RAISE
            class CustomSchema(Schema):
                class Meta:
                    unknown = RAISE
                required_field = fields.Str(required=True)
                resource = fields.Str(required=True)
            """
            )
        )
        f.flush()
        yield
    os.remove(custom_schema)
    del sys.modules["template.piperci_flask.function.custom_schema"]


@pytest.fixture
def task_delegation_response(config):
    introspect = {"json": {}}

    def callback(request):
        body = json.loads(request.body)
        introspect["json"] = body
        try:
            marshaller = TaskMarshaller(body)
            marshaller.enforce("pre_task")
            introspect["marshaller"] = True
            return 200, {}, json.dumps({"success": "valid json"})
        except MarshallError as e:
            introspect["marshaller"] = False
            return 422, {}, json.dumps(e.errors.emit())
        except Exception as e:
            introspect["marshaller"] = e
            return (400, {}, json.dumps({"errors": [{
                "faas_app:main_route": f"Unknown error processing function"}]})
                    )

    return {
        "args": [responses.POST,
                 config["executor_url"].format(endpoint=config["endpoint"])],
        "kwargs": {"callback": callback},
        "introspect": introspect
        }


@pytest.fixture(scope="function")
def app(monkeypatch):
    custom_module_config = {
        "custom_schema_module": "custom_schema",
        "custom_schema_class": "CustomSchema",
    }
    from template.piperci_flask.function.config import Config

    x = Config.copy()
    x.update(custom_module_config)
    monkeypatch.setattr("template.piperci_flask.function.models.Config", x)

    from template.piperci_flask.function import faas_app

    yield faas_app.app


@pytest.fixture
def config(common_headers):
    cfg = {
        "gman": {"url": "http://172.17.0.1:8089"},
        "storage": {
            "url": "172.17.0.1:9000",
            "access_key_secret": "/var/openfaas/secrets/access-key",
            "secret_key_secret": "/var/openfaas/secrets/secret-key",
            "access_key": "",
            "secret_key": "",
        },
        "name": "piperci-flask",
        "executor_url": "http://172.17.0.1:8080/async-function/piperci-flask-executor",
        "type": "gateway",
        "custom_schema_module": "",
        "custom_schema_class": "",
        "endpoint": common_headers["X-Forwarded-Host"]
    }
    return cfg


@responses.activate
def test_loads_custom_schema(
    client,
    mocker,
    common_headers,
    standard_environ,
    single_task_run,
    replace_file
):
    resp = client.post("/gateway", json=single_task_run)
    errors = resp.json["errors"]["config"]
    assert "resource" in errors and "required_field" in errors
    assert resp.status_code == 422
