import pytest
import responses

from requests.exceptions import RequestException

from piperci.task.exceptions import PiperError

from template.piperci_flask.function.config import Config


@pytest.fixture()
def app():
    from template.piperci_flask.function import faas_app

    return faas_app.app


@pytest.fixture
def config(common_headers):
    Config["endpoint"] = common_headers["X-Forwarded-Host"]
    return Config


@responses.activate
@pytest.mark.parametrize("request_entrypoint", ["gateway", "executor"])
def test_valid_routes_respond(
    client,
    common_headers,
    standard_environ,
    single_task_run,
    request_entrypoint
):
    resp = client.post(request_entrypoint, json=single_task_run, headers=common_headers)

    assert "task" in resp.json
    assert resp.status_code == 200


@pytest.mark.parametrize("task_config", [
    {},
    {"ingored_conf_val": ["a/", "b/"]}
])
@responses.activate
def test_gateway_config_values(
    client,
    common_headers,
    standard_environ,
    single_task_run,
    task_config
):

    single_task_run["config"] = task_config

    resp = client.post("/gateway", json=single_task_run, headers=common_headers)

    assert resp.status_code == 200
    assert "config" in (standard_environ["task_delegation_response"]
                        ["introspect"]["json"])
    assert (standard_environ["task_delegation_response"]
            ["introspect"]["marshaller"]) is not False, (
        "failed to pass executor marshaller"
    )


@responses.activate
def test_gateway_validate_only(
    client,
    mocker,
    common_headers,
    standard_environ,
    single_task_run
):
    m_gate = mocker.patch("template.piperci_flask.function.handler.gateway")
    m_exec = mocker.patch("template.piperci_flask.function.handler.executor")

    single_task_run["only_validate"] = True
    resp = client.post("/gateway", json=single_task_run, headers=common_headers)

    assert resp.status_code == 200
    assert "task" in resp.json
    m_gate.assert_not_called
    m_exec.assert_not_called


@responses.activate
def test_gateway_thread_id_is_none(
    client,
    common_headers,
    standard_environ,
    single_task_run
):

    single_task_run["thread_id"] = None
    resp = client.post("/gateway", json=single_task_run, headers=common_headers)

    assert resp.status_code == 200
    assert "task" in resp.json


@responses.activate
def test_missing_json_raises_error(
    client, config, request_ctx, standard_environ
):

    resp = client.post("gateway",
                       headers={"X-Forwarded-Host": request_ctx.request.host_url})

    assert resp.status_code == 400
    assert (resp.json["errors"]["faas_app:main_route"][0]
            == "JSON must be supplied in request")


def test_piperci_task_creation_fail_raises_error(
    client,
    mocker,
    common_headers,
    standard_environ,
    single_task_run
):

    mocker.patch(
        "template.piperci_flask.function.faas_app.ThisTask",
        autospec=True,
        side_effect=PiperError,
    )

    resp = client.post("/gateway", json=single_task_run, headers=common_headers)

    assert resp.status_code == 400


def test_catch_basic_marshall_errors(
    client,
    mocker,
    common_headers,
    standard_environ,
):

    invalid_schema = {
        "project": 123,
        "run_id": ["1234"],
        "stage": {"key": "tox_tests"},
        "thread_id": "1234",
        "task_id": 1234,
        "caller": None,
        "artifacts": ["abc"]
    }

    resp = client.post(
        "/gateway",
        json=invalid_schema,
        headers=common_headers
        )

    assert resp.status_code == 422
    errors = resp.json["errors"]

    assert "Not a valid SRI" in errors["artifacts"]["0"]
    assert "Missing data for required field." in errors["config"]
    assert "Missing data for required field." in errors["parent_id"]
    assert "Not a valid string." in errors["project"]
    assert "Not a valid string." in errors["run_id"]
    assert "Not a valid string." in errors["stage"]
    assert "Not a valid UUID." in errors["thread_id"]


@responses.activate
@pytest.mark.parametrize("params",
                         [["parent_id",
                           "63c52b8c-947d-479d-9bda-a3fcade649e2",
                           "get_task"],
                          ["run_id", "my_run", "get_run"],
                          ["thread_id",
                           "63c52b8c-947d-479d-9bda-a3fcade649e2",
                           "get_thread"]
                          ])
def test_catch_parent_id_dne(
    client,
    mocker,
    common_headers,
    standard_environ,
    single_task_run,
    params
):

    single_task_run[params[0]] = params[1]

    def _mock(*args, **kwargs):
        if params[0] == "parent_id":
            field = "task_id"
        else:
            field = params[0]
        if str(kwargs[field]) == single_task_run[params[0]]:
            return None
        else:
            return kwargs[params[0]]

    setattr(standard_environ["mocks"]["gman_client"], params[2], _mock)

    resp = client.post("/gateway", json=single_task_run, headers=common_headers)

    assert resp.status_code == 422
    assert "does not exist" in resp.json["errors"][params[0]][0]


@responses.activate
@pytest.mark.parametrize("method", ["get_task", "get_thread", "get_run"])
def test_catch_gman_client_requests_exception(
    client,
    mocker,
    common_headers,
    standard_environ,
    single_task_run,
    method
):

    def _raise_exception(*args, **kwargs):
        raise RequestException("Testing exception handling")

    setattr(standard_environ["mocks"]["gman_client"], method, _raise_exception)

    resp = client.post("/gateway", json=single_task_run, headers=common_headers)

    assert resp.status_code == 422


@responses.activate
def test_handler_exception_raises_error(
    client,
    mocker,
    common_headers,
    standard_environ,
    single_task_run
):

    mocker.patch(
        "template.piperci_flask.function.handler.gateway", side_effect=Exception
    )

    resp = client.post(
        "/gateway",
        json=single_task_run,
        headers=common_headers
    )

    assert resp.status_code == 400
    assert ("Unknown error processing function"
            in resp.json["errors"]["task.fail"][0]
            and "handler.gateway" in resp.json["errors"]["task.fail"][0])


# This test increases coverage, but doesn't have a good way to assert the value was set
@responses.activate
def test_transfer_encoding_chunked(
    client,
    mocker,
    common_headers,
    standard_environ,
    single_task_run
):
    common_headers["Transfer-Encoding"] = "chunked"

    resp = client.post("/gateway", json=single_task_run, headers=common_headers)

    assert resp.status_code == 200


@responses.activate
@pytest.mark.parametrize("request_entrypoint", ["gateway", "executor"])
def test_invalid_artifact_sri(
    client,
    common_headers,
    standard_environ,
    single_task_run,
    request_entrypoint
):

    single_task_run["artifacts"] = [
        "abc123",
        "sha384-oqVuAfXRKap7fdgcCY5uykM6+R9GqQ8K/uxy9rx7HNQlGYl1kPzQho1wx4JwY8wC"]

    resp = client.post(request_entrypoint, json=single_task_run, headers=common_headers)

    assert resp.status_code == 422


@responses.activate
@pytest.mark.parametrize("request_entrypoint", ["gateway", "executor"])
def test_empty_artifacts(
    client,
    common_headers,
    standard_environ,
    single_task_run,
    request_entrypoint
):

    single_task_run["artifacts"] = []

    resp = client.post(request_entrypoint, json=single_task_run, headers=common_headers)

    assert resp.status_code == 200

    # ensure delegation called with empty artifacts list
    if request_entrypoint == "gateway":
        assert (standard_environ["task_delegation_response"]
                ["introspect"]["json"]["artifacts"] == [])


@responses.activate
@pytest.mark.parametrize("request_entrypoint", ["gateway", "executor"])
def test_del_artifacts(
    client,
    common_headers,
    standard_environ,
    single_task_run,
    request_entrypoint
):

    del single_task_run["artifacts"]

    resp = client.post(request_entrypoint, json=single_task_run, headers=common_headers)

    assert resp.status_code == 200

    # ensure delegation called with empty artifacts list
    if request_entrypoint == "gateway":
        assert (standard_environ["task_delegation_response"]
                ["introspect"]["json"]["artifacts"] == [])


@responses.activate
def test_marshaller_unknown_exception(
    client,
    mocker,
    common_headers,
    standard_environ,
    single_task_run
):
    mocker.patch("template.piperci_flask.function.faas_app.TaskMarshaller.enforce",
                 side_effect=Exception("Mocked exception failure"))
    resp = client.post("/gateway", json=single_task_run, headers=common_headers)

    assert resp.status_code == 400
    assert "Mocked exception failure" in resp.json["errors"]["faas_app:main_route"][0]
